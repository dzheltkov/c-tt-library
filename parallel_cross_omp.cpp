#include "parallel_cross_omp.h"
#include <iostream>
using namespace std;
TCross_Parallel_v1_Parameters::TCross_Parallel_v1_Parameters():tolerance(0.0), maximal_iterations_number(1), number_of_checked_elements(1), max_rank(1), rank_increase(1), stop_rank(0), number_of_preserved_elements(0), preserved_elements_indeces(NULL), memory_strategy(true), start_from_column(true)
{
}

const double * TCross_Parallel_v1::export_C()
{
    return C;
}

const double * TCross_Parallel_v1::export_hat_A_inv()
{
    return hat_A_inv;
}

const double * TCross_Parallel_v1::export_RT()
{
    return RT;
}

using namespace std;

void TCross_Parallel_v1::get_diff_column(const int &j, const TCross_Parallel_v1_Work_Data & work_data, double *&result)
{
    int i;
    #pragma omp parallel for
    for (i = ((int) 0); i < this->rows_number; i++)
    {
        if (!work_data.IR[i])
        {
            result[i] = work_data.matrix->value(i, j);
        }
    }
    copy(this->get_rows_number(), result, 1, C + this->rank*this->get_rows_number(), 1);
    gemv(CblasColMajor, CblasNoTrans, this->get_rows_number(), this->rank, - 1.0, U, this->get_rows_number(), V + j, this->get_columns_number(), 1.0, result, 1);
    return;
}

void TCross_Parallel_v1::get_diff_row(const int &i, const TCross_Parallel_v1_Work_Data & work_data, double *&result)
{
    int j;
    #pragma omp parallel for
    for (j = ((int) 0); j < this->columns_number; j++)
    {
        if (!work_data.J[j])
        {
            result[j] = work_data.matrix->value(i, j);
        }
    }
    copy(this->get_columns_number(), result, 1, RT + this->rank*this->get_columns_number(), 1);
    gemv(CblasColMajor, CblasNoTrans, this->get_columns_number(), this->rank, - 1.0, V, this->get_columns_number(), U + i, this->get_rows_number(), 1.0, result, 1);
    return;
}

TCross_Parallel_v1_Work_Data::TCross_Parallel_v1_Work_Data(TMatrix *original_matrix, TMatrix *approximated_matrix): max_volume(1), work_matrix(original_matrix, approximated_matrix)
{
    IR = (bool *) malloc(original_matrix->get_rows_number()*sizeof(bool));
    #pragma omp parallel for
    for (int i = ((int) 0); i < original_matrix->get_rows_number(); i++) IR[i] = false;
    J = (bool *) malloc(original_matrix->get_columns_number()*sizeof(bool));
    #pragma omp parallel for
    for (int j = ((int) 0); j < original_matrix->get_columns_number(); j++) J[j] = false;
    global_max = 0.0;
    matrix = original_matrix;
    omp_column_threads_num = omp_get_max_threads();
    omp_row_threads_num = omp_column_threads_num;
    int avg_column = (original_matrix->get_rows_number() - 1) / omp_column_threads_num + 1;
    int avg_row = (original_matrix->get_columns_number() - 1) / omp_row_threads_num + 1;
    omp_column_threads_num = (original_matrix->get_rows_number() - 1) / avg_column + 1;
    omp_row_threads_num = (original_matrix->get_columns_number() - 1) / avg_row + 1;
    omp_column_start = (int *) malloc(omp_column_threads_num * sizeof(int));
    omp_column_num = (int *) malloc(omp_column_threads_num * sizeof(int));
    omp_row_start = (int *) malloc(omp_row_threads_num * sizeof(int));
    omp_row_num = (int *) malloc(omp_row_threads_num * sizeof(int));
    omp_column_start[((int) 0)] = ((int) 0);
    omp_row_start[((int) 0)] = ((int) 0);
    for (int i = 1; i < omp_column_threads_num; i++)
    {
        omp_column_start[i] = omp_column_start[i - 1] + avg_column;
        omp_column_num[i - 1] = avg_column;
    }
    omp_column_num[omp_column_threads_num - 1] = original_matrix->get_rows_number() - omp_column_start[omp_column_threads_num - 1];
    for (int i = 1; i < omp_row_threads_num; i++)
    {
        omp_row_start[i] = omp_row_start[i - 1] + avg_row;
        omp_row_num[i - 1] = avg_row;
    }
    omp_row_num[omp_row_threads_num - 1] = original_matrix->get_columns_number() - omp_row_start[omp_row_threads_num - 1];
    preserved_elements = NULL;
}

TCross_Parallel_v1_Work_Data::~TCross_Parallel_v1_Work_Data()
{
    free(IR);
    free(J);
    free(current_row);
    free(current_column);
    free(omp_column_start);
    free(omp_column_num);
    free(omp_row_start);
    free(omp_row_num);
    free(preserved_elements);
}

void TCross_Parallel_v1::Prepare_Data(TCross_Parallel_v1_Work_Data &work_data,  const TCross_Parallel_v1_Parameters & parameters)
{
    norm = 0.0;
    free(U);
    free(V);
    free(C);
    free(hat_A_inv);
    free(RT);
    U = NULL;
    V = NULL;
    C = NULL;
    hat_A_inv = NULL;
    RT = NULL;
    free(rows_numbers);
    free(columns_numbers);
    rows_numbers = NULL;
    columns_numbers = NULL;
    tolerance = parameters.tolerance;
    work_data.parameters = parameters;
    if (work_data.parameters.max_rank <= ((int) 0))
    {
        work_data.parameters.max_rank = 1;
    }
    if ((work_data.parameters.rank_increase) <= ((int) 0) && (work_data.parameters.memory_strategy))
    {
        work_data.parameters.rank_increase = 1;
    }
    else if ((work_data.parameters.rank_increase) <= 1 && (!work_data.parameters.memory_strategy))
    {
        work_data.parameters.rank_increase = 1 + 1;
    }
    #pragma omp parallel sections
    {
        #pragma omp section
            U = (double *) calloc(work_data.parameters.max_rank * this->get_rows_number(), sizeof(double));
        #pragma omp section
            C = (double *) calloc(work_data.parameters.max_rank * this->get_rows_number(), sizeof(double));
        #pragma omp section
            V = (double *) calloc(work_data.parameters.max_rank * this->get_columns_number(), sizeof(double));
        #pragma omp section
            RT = (double *) calloc(work_data.parameters.max_rank * this->get_columns_number(), sizeof(double));
        #pragma omp section
            rows_numbers = (int *) realloc(rows_numbers, work_data.parameters.max_rank*sizeof(int));
        #pragma omp section
            columns_numbers = (int *) realloc(columns_numbers, work_data.parameters.max_rank*sizeof(int));
    }
    this->rank = ((int) 0);
    work_data.preserved_elements = (double *) malloc(work_data.parameters.number_of_preserved_elements*sizeof(double));
#pragma omp parallel for
    for (int i = 0; i < work_data.parameters.number_of_preserved_elements; i++)
    {
        work_data.preserved_elements[i] = work_data.matrix->value(work_data.parameters.preserved_elements_indeces[2*i], work_data.parameters.preserved_elements_indeces[2*i + 1]);
    }
    /*cout << "Preserved elements:" << endl;
    for (int i = 0; i < work_data.parameters.number_of_preserved_elements; i++)
    {
        cout << tan(M_PI / 2 - work_data.preserved_elements[i]) << endl;
    }*/
}

TCross_Parallel_v1::TCross_Parallel_v1(): TCross_Base<TCross_Parallel_v1_Work_Data, TCross_Parallel_v1_Parameters>()
{
    U = NULL;
    V = NULL;
    C = NULL;
    hat_A_inv = NULL;
    RT = NULL;
    rows_numbers = NULL;
    columns_numbers = NULL;
    this->rank = ((int) 0);
};

TCross_Parallel_v1::~TCross_Parallel_v1()
{
    free(U);
    free(V);
    free(C);
    free(hat_A_inv);
    free(RT);
    free(rows_numbers);
    free(columns_numbers);
};

double TCross_Parallel_v1::value(const int &i, const int &j)
{
    return dot_u(this->rank, U + i, this->get_rows_number(), V + j, this->get_columns_number());
}

void TCross_Parallel_v1::Search_Max_Volume(TCross_Parallel_v1_Work_Data &work_data)
{
    if (this->rank == min(this->get_rows_number(), this->get_columns_number())) return;
    if (this->rank == work_data.parameters.stop_rank) return;
    if (this->rank >= work_data.parameters.max_rank)
    {
        if (work_data.parameters.memory_strategy)
        {
            work_data.parameters.max_rank += work_data.parameters.rank_increase;
        }
        else
        {
            work_data.parameters.max_rank *= work_data.parameters.rank_increase;
        }
        #pragma omp parallel sections
        {
            #pragma omp section
            {
                U = (double *) realloc(U, work_data.parameters.max_rank * this->get_rows_number() * sizeof(double));
                for (int i = rank*this->get_rows_number(); i < work_data.parameters.max_rank * this->get_rows_number(); i++) U[i] = 0.0;
            }
            #pragma omp section
            {
                C = (double *) realloc(C, work_data.parameters.max_rank * this->get_rows_number() * sizeof(double));
                for (int i = rank*this->get_rows_number(); i < work_data.parameters.max_rank * this->get_rows_number(); i++) C[i] = 0.0;
            }
            #pragma omp section
            {
                V = (double *) realloc(V, work_data.parameters.max_rank * this->get_columns_number() * sizeof(double));
                for (int i = rank*this->get_columns_number(); i < work_data.parameters.max_rank * this->get_columns_number(); i++) V[i] = 0.0;
            }
            #pragma omp section
            {
                RT = (double *) realloc(RT, work_data.parameters.max_rank * this->get_columns_number() * sizeof(double));
                for (int i = rank*this->get_columns_number(); i < work_data.parameters.max_rank * this->get_columns_number(); i++) RT[i] = 0.0;
            }
            #pragma omp section
                rows_numbers = (int *) realloc(rows_numbers, work_data.parameters.max_rank*sizeof(int));
            #pragma omp section
                columns_numbers = (int *) realloc(columns_numbers, work_data.parameters.max_rank*sizeof(int));
        }
        #pragma omp barrier
    }
    work_data.current_column = U + this->rank * this->get_rows_number();
    work_data.current_row = V + this->rank * this->get_columns_number();
    //int ik = rand() % (this->get_rows_number() - this->rank), jk = rand() % (this->get_columns_number() - this->rank);
    int ik(0), jk(0);
    viRngUniform(VSL_RNG_METHOD_UNIFORM_STD, work_data.parameters.stream, 1, &ik, 0, this->get_rows_number() - this->rank);
    viRngUniform(VSL_RNG_METHOD_UNIFORM_STD, work_data.parameters.stream, 1, &jk, 0, this->get_columns_number() - this->rank);
    int i(0), j(0);
    int ia(0), ja(0);
    #pragma omp parallel sections
    {
        #pragma omp section
            while (ia <= ik)
            {
                if (!work_data.IR[i]) ia++;
                i++;
            }
        #pragma omp section
            while (ja <= jk)
            {
                if (!work_data.J[j]) ja++;
                j++;
            }
    }
    i--;
    j--;
    #pragma omp barrier
    work_data.max_volume.set(work_data.work_matrix.value(i, j), &i, &j);
    int iters = ((int) 0), prev_column(j), prev_row(i);
    bool b;
    if (work_data.parameters.start_from_column)
    {
        do
        {
            b = (work_data.max_volume.get_column_position() == prev_column);
            if (iters == 0)
            {
                b = false;
            }
            if (!b)
            {
                j = work_data.max_volume.get_column_position();
                get_diff_column(j, work_data, work_data.current_column);
                i = work_data.max_volume.get_row_position();
                int ind[work_data.omp_column_threads_num];
                #pragma omp parallel for
                for (int o = ((int) 0); o < work_data.omp_column_threads_num; o++)
                {
                    ind[o] = i;
                    for (int k = work_data.omp_column_start[o]; k < work_data.omp_column_start[o] + work_data.omp_column_num[o]; k++)
                    {
                        if (!work_data.IR[k])
                        {
                            if (abs(work_data.current_column[ind[o]]) < abs(work_data.current_column[k]))
                            {
                                ind[o] = k;
                            }
                        }
                    }
                }
                #pragma omp barrier
                i = ind[((int) 0)];
                for (int o = 1; o < work_data.omp_column_threads_num; o++)
                {
                    if (abs(work_data.current_column[i]) < abs(work_data.current_column[ind[o]]))
                    {
                        i = ind[o];
                    }
                }
                work_data.max_volume.set(work_data.current_column[i], &i, &j);
                prev_column = work_data.max_volume.get_column_position();
            }
            b = (work_data.max_volume.get_row_position() == prev_row);
            if (iters == 0)
            {
                b = false;
            }
            if (!b)
            {
                i = work_data.max_volume.get_row_position();
                get_diff_row(i, work_data, work_data.current_row);
                j = work_data.max_volume.get_column_position();
                int ind[work_data.omp_row_threads_num];
                #pragma omp parallel for
                for (int o = ((int) 0); o < work_data.omp_row_threads_num; o++)
                {
                    ind[o] = j;
                    for (int k = work_data.omp_row_start[o]; k < work_data.omp_row_start[o] + work_data.omp_row_num[o]; k++)
                    {
                        if (!work_data.J[k])
                        {
                            if (abs(work_data.current_row[ind[o]]) < abs(work_data.current_row[k]))
                            {
                                ind[o] = k;
                            }
                        }
                    }
                }
                #pragma omp barrier
                j = ind[((int) 0)];
                for (int o = 1; o < work_data.omp_row_threads_num; o++)
                {
                    if (abs(work_data.current_row[j]) < abs(work_data.current_row[ind[o]]))
                    {
                        j = ind[o];
                    }
                }
                work_data.max_volume.set(work_data.current_row[j], &i, &j);
                prev_row = work_data.max_volume.get_row_position();
            }
            iters++;
        } while ((!b) || ((work_data.parameters.maximal_iterations_number > 0) && (iters < work_data.parameters.maximal_iterations_number)));
    }
    else
    {
        do
        {
            b = (work_data.max_volume.get_row_position() == prev_row);
            if (iters == 0)
            {
                b = false;
            }
            if (!b)
            {
                i = work_data.max_volume.get_row_position();
                get_diff_row(i, work_data, work_data.current_row);
                j = work_data.max_volume.get_column_position();
                int ind[work_data.omp_row_threads_num];
                #pragma omp parallel for
                for (int o = ((int) 0); o < work_data.omp_row_threads_num; o++)
                {
                    ind[o] = j;
                    for (int k = work_data.omp_row_start[o]; k < work_data.omp_row_start[o] + work_data.omp_row_num[o]; k++)
                    {
                        if (!work_data.J[k])
                        {
                            if (abs(work_data.current_row[ind[o]]) < abs(work_data.current_row[k]))
                            {
                                ind[o] = k;
                            }
                        }
                    }
                }
                #pragma omp barrier
                j = ind[((int) 0)];
                for (int o = 1; o < work_data.omp_row_threads_num; o++)
                {
                    if (abs(work_data.current_row[j]) < abs(work_data.current_row[ind[o]]))
                    {
                        j = ind[o];
                    }
                }
                work_data.max_volume.set(work_data.current_row[j], &i, &j);
                prev_row = work_data.max_volume.get_row_position();
            }
            b = (work_data.max_volume.get_column_position() == prev_column);
            if (iters == 0)
            {
                b = false;
            }
            if (!b)
            {
                j = work_data.max_volume.get_column_position();
                get_diff_column(j, work_data, work_data.current_column);
                i = work_data.max_volume.get_row_position();
                int ind[work_data.omp_column_threads_num];
                #pragma omp parallel for
                for (int o = ((int) 0); o < work_data.omp_column_threads_num; o++)
                {
                    ind[o] = i;
                    for (int k = work_data.omp_column_start[o]; k < work_data.omp_column_start[o] + work_data.omp_column_num[o]; k++)
                    {
                        if (!work_data.IR[k])
                        {
                            if (abs(work_data.current_column[ind[o]]) < abs(work_data.current_column[k]))
                            {
                                ind[o] = k;
                            }
                        }
                    }
                }
                #pragma omp barrier
                i = ind[((int) 0)];
                for (int o = 1; o < work_data.omp_column_threads_num; o++)
                {
                    if (abs(work_data.current_column[i]) < abs(work_data.current_column[ind[o]]))
                    {
                        i = ind[o];
                    }
                }
                work_data.max_volume.set(work_data.current_column[i], &i, &j);
                prev_column = work_data.max_volume.get_column_position();
            }
            iters++;
        } while ((!b) || ((work_data.parameters.maximal_iterations_number > 0) && (iters < work_data.parameters.maximal_iterations_number)));
    }
    b = true;
    int k;
    if (work_data.parameters.number_of_preserved_elements > 0)
    {
        k = iamax(work_data.parameters.number_of_preserved_elements, work_data.preserved_elements, 1);
        i = work_data.parameters.preserved_elements_indeces[2*k];
        j = work_data.parameters.preserved_elements_indeces[2*k + 1];
        b = work_data.IR[i] || work_data.J[j];
    }
    if (!b)
    {
        double volume = work_data.max_volume.get_volume();
        int i1 = work_data.max_volume.get_row_position(), j1 = work_data.max_volume.get_column_position();
        work_data.max_volume.set(work_data.preserved_elements[k], &i, &j);
        iters = 0;
        prev_column = j;
        prev_row = i;
        if (work_data.parameters.start_from_column)
        {
            do
            {
                b = (work_data.max_volume.get_column_position() == prev_column);
                if (iters == 0)
                {
                    b = false;
                }
                if (!b)
                {
                    j = work_data.max_volume.get_column_position();
                    get_diff_column(j, work_data, work_data.current_column);
                    i = work_data.max_volume.get_row_position();
                    int ind[work_data.omp_column_threads_num];
#pragma omp parallel for
                    for (int o = ((int) 0); o < work_data.omp_column_threads_num; o++)
                    {
                        ind[o] = i;
                        for (int k = work_data.omp_column_start[o]; k < work_data.omp_column_start[o] + work_data.omp_column_num[o]; k++)
                        {
                            if (!work_data.IR[k])
                            {
                                if (abs(work_data.current_column[ind[o]]) < abs(work_data.current_column[k]))
                                {
                                    ind[o] = k;
                                }
                            }
                        }
                    }
#pragma omp barrier
                    i = ind[((int) 0)];
                    for (int o = 1; o < work_data.omp_column_threads_num; o++)
                    {
                        if (abs(work_data.current_column[i]) < abs(work_data.current_column[ind[o]]))
                        {
                            i = ind[o];
                        }
                    }
                    work_data.max_volume.set(work_data.current_column[i], &i, &j);
                    prev_column = work_data.max_volume.get_column_position();
                }
                b = (work_data.max_volume.get_row_position() == prev_row);
                if (iters == 0)
                {
                    b = false;
                }
                if (!b)
                {
                    i = work_data.max_volume.get_row_position();
                    get_diff_row(i, work_data, work_data.current_row);
                    j = work_data.max_volume.get_column_position();
                    int ind[work_data.omp_row_threads_num];
#pragma omp parallel for
                    for (int o = ((int) 0); o < work_data.omp_row_threads_num; o++)
                    {
                        ind[o] = j;
                        for (int k = work_data.omp_row_start[o]; k < work_data.omp_row_start[o] + work_data.omp_row_num[o]; k++)
                        {
                            if (!work_data.J[k])
                            {
                                if (abs(work_data.current_row[ind[o]]) < abs(work_data.current_row[k]))
                                {
                                    ind[o] = k;
                                }
                            }
                        }
                    }
#pragma omp barrier
                    j = ind[((int) 0)];
                    for (int o = 1; o < work_data.omp_row_threads_num; o++)
                    {
                        if (abs(work_data.current_row[j]) < abs(work_data.current_row[ind[o]]))
                        {
                            j = ind[o];
                        }
                    }
                    work_data.max_volume.set(work_data.current_row[j], &i, &j);
                    prev_row = work_data.max_volume.get_row_position();
                }
                iters++;
            } while ((!b) || ((work_data.parameters.maximal_iterations_number > 0) && (iters < work_data.parameters.maximal_iterations_number)));
        }
        else
        {
            do
            {
                b = (work_data.max_volume.get_row_position() == prev_row);
                if (iters == 0)
                {
                    b = false;
                }
                if (!b)
                {
                    i = work_data.max_volume.get_row_position();
                    get_diff_row(i, work_data, work_data.current_row);
                    j = work_data.max_volume.get_column_position();
                    int ind[work_data.omp_row_threads_num];
#pragma omp parallel for
                    for (int o = ((int) 0); o < work_data.omp_row_threads_num; o++)
                    {
                        ind[o] = j;
                        for (int k = work_data.omp_row_start[o]; k < work_data.omp_row_start[o] + work_data.omp_row_num[o]; k++)
                        {
                            if (!work_data.J[k])
                            {
                                if (abs(work_data.current_row[ind[o]]) < abs(work_data.current_row[k]))
                                {
                                    ind[o] = k;
                                }
                            }
                        }
                    }
#pragma omp barrier
                    j = ind[((int) 0)];
                    for (int o = 1; o < work_data.omp_row_threads_num; o++)
                    {
                        if (abs(work_data.current_row[j]) < abs(work_data.current_row[ind[o]]))
                        {
                            j = ind[o];
                        }
                    }
                    work_data.max_volume.set(work_data.current_row[j], &i, &j);
                    prev_row = work_data.max_volume.get_row_position();
                }
                b = (work_data.max_volume.get_column_position() == prev_column);
                if (iters == 0)
                {
                    b = false;
                }
                if (!b)
                {
                    j = work_data.max_volume.get_column_position();
                    get_diff_column(j, work_data, work_data.current_column);
                    i = work_data.max_volume.get_row_position();
                    int ind[work_data.omp_column_threads_num];
#pragma omp parallel for
                    for (int o = ((int) 0); o < work_data.omp_column_threads_num; o++)
                    {
                        ind[o] = i;
                        for (int k = work_data.omp_column_start[o]; k < work_data.omp_column_start[o] + work_data.omp_column_num[o]; k++)
                        {
                            if (!work_data.IR[k])
                            {
                                if (abs(work_data.current_column[ind[o]]) < abs(work_data.current_column[k]))
                                {
                                    ind[o] = k;
                                }
                            }
                        }
                    }
#pragma omp barrier
                    i = ind[((int) 0)];
                    for (int o = 1; o < work_data.omp_column_threads_num; o++)
                    {
                        if (abs(work_data.current_column[i]) < abs(work_data.current_column[ind[o]]))
                        {
                            i = ind[o];
                        }
                    }
                    work_data.max_volume.set(work_data.current_column[i], &i, &j);
                    prev_column = work_data.max_volume.get_column_position();
                }
                iters++;
            } while ((!b) || ((work_data.parameters.maximal_iterations_number > 0) && (iters < work_data.parameters.maximal_iterations_number)));
        } 
        if (abs(volume) > abs(work_data.max_volume.get_volume()))
        {
            work_data.max_volume.set(volume, &i1, &j1);
        }

    }

    if (prev_column != work_data.max_volume.get_column_position())
    {
        get_diff_column(work_data.max_volume.get_column_position(), work_data, work_data.current_column);
    }
    if (prev_row != work_data.max_volume.get_row_position())
    {
        get_diff_row(work_data.max_volume.get_row_position(), work_data, work_data.current_row);
    }

}

bool TCross_Parallel_v1::Stopping_Criteria(TCross_Parallel_v1_Work_Data &work_data){
    if (abs(work_data.max_volume.get_volume()) > abs(work_data.global_max))
    {
        work_data.global_max = abs(work_data.max_volume.get_volume());
    }
    if (((work_data.parameters.stop_rank > 0) && (this->rank >= work_data.parameters.stop_rank)) ||(sqrt(real(norm))*abs(tolerance) >= abs(work_data.max_volume.get_volume())*sqrt(this->get_columns_number()-this->rank)*sqrt(this->get_rows_number()-this->rank)))
    {
        #pragma omp parallel sections
        {
            #pragma omp section
                U = (double *) realloc(U, this->rank * this->get_rows_number() * sizeof(double));
            #pragma omp section
                V = (double *) realloc(V, this->rank * this->get_columns_number() * sizeof(double));
            #pragma omp section
                C = (double *) realloc(C, this->rank * this->get_rows_number() * sizeof(double));
            #pragma omp section
                RT = (double *) realloc(RT, this->rank * this->get_columns_number() * sizeof(double));
            #pragma omp section
                rows_numbers = (int *) realloc(rows_numbers ,this->rank*sizeof(int));
            #pragma omp section
                columns_numbers = (int *) realloc(columns_numbers,this->rank*sizeof(int));
        }
        #pragma omp barrier
        double *hat_A = (double *) calloc(this->rank * this->rank, sizeof(double));
        #pragma omp parallel for
        for (int k = ((int) 0); k < this->rank; k++)
        {
            copy(this->rank, RT + columns_numbers[k], this->get_columns_number(), hat_A + k*this->rank, 1);
        }
        hat_A_inv = pseudoinverse(1e-8, this->rank, this->rank, hat_A);
        free(hat_A);
        work_data.current_row = NULL;
        work_data.current_column = NULL;
        return true;
    }
    else
    {
        return false;
    }
}

void TCross_Parallel_v1::Update_Cross(TCross_Parallel_v1_Work_Data &work_data)
{
    double column_factor(1.0/sqrt(abs(work_data.max_volume.get_volume()))), row_factor = 1.0 / (work_data.max_volume.get_volume() * column_factor);
    #pragma omp parallel sections
    {
        #pragma omp section
        {
            #pragma omp parallel for
            for (int i = ((int) 0); i < this->get_rows_number(); i++)
            {
                if (!work_data.IR[i])
                {
                    work_data.current_column[i] *= column_factor;
                }
                else
                {
                    work_data.current_column[i] = 0.0;
                }
            }
        }
        #pragma omp section
        {
            #pragma omp parallel for
            for (int j = ((int) 0); j < this->get_columns_number(); j++)
            {
                if (!work_data.J[j])
                {
                    work_data.current_row[j] *= row_factor;
                }
                else
                {
                    work_data.current_row[j] = 0.0;
                }
            }
        }
    }
#pragma omp parallel for
    for (int i = 0; i < work_data.parameters.number_of_preserved_elements; i++)
    {
        work_data.preserved_elements[i] -= work_data.current_column[work_data.parameters.preserved_elements_indeces[2*i]] * work_data.current_row[work_data.parameters.preserved_elements_indeces[2*i + 1]];
    }
    work_data.IR[work_data.max_volume.get_row_position()] = true;
    work_data.J[work_data.max_volume.get_column_position()] = true;
    rows_numbers[this->rank] = work_data.max_volume.get_row_position();
    columns_numbers[this->rank] = work_data.max_volume.get_column_position();
    #pragma omp parallel sections
    {
        #pragma omp section
        {
            #pragma omp parallel for
            for (int i = ((int) 0); i < this->rank; i++)
            {
                RT[this->rank*this->get_columns_number() + columns_numbers[i]] = this->value(work_data.max_volume.get_row_position(), columns_numbers[i]);
            }
        }
        #pragma omp section
        {
            #pragma omp parallel for
            for (int i = ((int) 0); i < this->rank; i++)
            {
                C[this->rank*this->get_rows_number() + rows_numbers[i]] = this->value(rows_numbers[i], work_data.max_volume.get_column_position());
            }
        }
    }
    double *b = (double *) calloc ((this->rank + 1), sizeof(double)), *x = (double *) calloc ((this->rank + 1), sizeof(double));
    gemv(CblasColMajor, CblasConjTrans, this->get_rows_number(), this->rank + 1, 1.0, U, this->get_rows_number(), work_data.current_column, 1, 0.0, b, 1);
    gemv(CblasColMajor, CblasConjTrans, this->get_columns_number(), this->rank + 1, 1.0, V, this->get_columns_number(), work_data.current_row, 1, 0.0, x, 1);
    norm += 2.0*real(dot_c(this->rank, b, 1, x, 1)) + b[this->rank]*x[this->rank];
    free(x);
    free(b);
    this->rank++;
    work_data.current_column = NULL;
    work_data.current_row = NULL;
}

int TCross_Parallel_v1::get_row_number(const int & k) const
{
    return rows_numbers[k];
}

int TCross_Parallel_v1::get_column_number(const int & k) const
{
    return columns_numbers[k];
}



double *TCross_Parallel_v1::smol_conv(double * &x)
{
// Discrete convolution : h[k] = \sum_{i=0}^k { A(i, k-i) * x[i]*x[k-i] }, where square matix A is approximated by cross-algorithm with factors U and V with sizes NxR
// A := U * transpose (V)
// A MUST be square!

// allocate memory
	int R = this->get_rank();
	int N = this->rows_number;
	double _Complex *Ubuff = (double _Complex *) malloc((this->get_rank()) * (2 * this->rows_number - 1) * sizeof(double _Complex));
	double _Complex *Vbuff = (double _Complex *) malloc((this->get_rank()) * (2 * this->columns_number - 1) * sizeof(double _Complex));
// preparations of arrays with elementwise products before convolution
	#pragma omp parallel for
	for (int j = int (0); j < R; j++)
	{
		for (int i = 0; i < 2 * N - 1; i++)
		{
			if (i < this->rows_number)
			{
				
				__real__ Ubuff[(2 * N - 1)  * j + i] = U[N * j + i] * x[i];
				__imag__ Ubuff[(2 * N - 1)  * j + i] = 0.0;

				__real__ Vbuff[(2 * N - 1) * j + i] = V[N * j + i] * x[i];
				__imag__ Vbuff[(2 * N - 1) * j + i] = 0.0;
			}
			else
			{
				__real__ Ubuff[(2 * N - 1) * j + i] = 0.0;
				__imag__ Ubuff[(2 * N - 1) * j + i] = 0.0;

				__real__ Vbuff[(2 * N - 1) * j + i] = 0.0;
				__imag__ Vbuff[(2 * N - 1) * j + i] = 0.0;
			}
		}

	}
// get FFTs
//FFT V
	DFTI_DESCRIPTOR_HANDLE my_desc_handle_V;
	DftiCreateDescriptor(&my_desc_handle_V, DFTI_DOUBLE, DFTI_COMPLEX, 1, 2 * N -1);

	DftiSetValue(my_desc_handle_V, DFTI_NUMBER_OF_TRANSFORMS , R);
	DftiSetValue(my_desc_handle_V, DFTI_INPUT_DISTANCE, 2 * N - 1);

	DftiCommitDescriptor(my_desc_handle_V);
	DftiComputeForward(my_desc_handle_V, Vbuff);
	DftiFreeDescriptor(&my_desc_handle_V);
//FFT U
	DFTI_DESCRIPTOR_HANDLE my_desc_handle_U;

	DftiCreateDescriptor(&my_desc_handle_U, DFTI_DOUBLE, DFTI_COMPLEX, 1, 2 * N - 1);

	DftiSetValue(my_desc_handle_U, DFTI_NUMBER_OF_TRANSFORMS , R);
	DftiSetValue(my_desc_handle_U, DFTI_INPUT_DISTANCE, 2 * N - 1);

	DftiCommitDescriptor(my_desc_handle_U);
	DftiComputeForward(my_desc_handle_U, Ubuff);
	DftiFreeDescriptor(&my_desc_handle_U);
// elementwise products after FFT. 
//result will be written in elements of array Vbuff
	#pragma omp parallel for
	for (int j = int(0); j < R; j++)
		for (int i = int(0); i < 2 * N - 1; i++)
			Vbuff[(2 * N - 1) * j + i] *= Ubuff[(2 * N - 1) * j + i];
	
// backward FFT and cutoff of extra elements and getting a resulting sum of 1d conv-resulting vectors
//
	DftiCreateDescriptor(&my_desc_handle_V, DFTI_DOUBLE, DFTI_COMPLEX, 1, 2 * N - 1);
	
	DftiSetValue(my_desc_handle_V, DFTI_NUMBER_OF_TRANSFORMS , R);
	DftiSetValue(my_desc_handle_V, DFTI_INPUT_DISTANCE, 2 * N - 1);
	
	DftiCommitDescriptor(my_desc_handle_V);
	DftiComputeBackward(my_desc_handle_V, Vbuff);
	DftiFreeDescriptor(&my_desc_handle_V);
//
	double *result = (double *)malloc( (2 * N - 1) * sizeof(double));

	#pragma omp parallel for
	for (int i = int(0); i < 2 * N - 1; i++)
	{
		result[i] = 0.0;
		for (int j = 0; j < R; j++)
			result[i] += __real__ Vbuff[(2 * N - 1) * j + i] / (2 * N - 1);
	}
			
//free memory
	free(Ubuff);
	free(Vbuff);
	Ubuff = NULL;
	Vbuff = NULL;
	//result[0] = 0.0;
	return result;
//	return NULL;
}

double *TCross_Parallel_v1::smol_conv_trapezoids(double * &x)
{
//Correct the convolution with kernel A=U * transpose(V) into trapecial-weights quadrature
	int R = this->get_rank();
	int N = this->rows_number;
	double *result = (double *)malloc (N *sizeof(double));
	free(result);
	result = this->smol_conv(x);
	result[0] = 0.0;
	#pragma omp parallel for
	for (int i = int (1); i < N; i++)
		for(int j = 0; j < R; j++)
			result[i] -= 0.5 * (U[N * j + i] * V[N * j] * x[0] * x[i] + U[N * j] * V[N * j + i] * x[i] * x[0]);
	result[0] = 0.0;
	return result;
}

double *TCross_Parallel_v1::smol_conv_discrete(double *&x)
{
// correct the convolution to the discrete case , thus we need to shift the whole vector down and put the 0 in the first component
	int N = this->rows_number;
	double *result = (double *)malloc (N * sizeof(double));
	free(result);
	result = this ->smol_conv(x);
	double bubble1, bubble2;
	bubble1 = result[0];
	for (int i = 0; i < (N - 1); i++)
	{
		bubble2 = result[i + 1];
		result[i + 1] = bubble1;
		bubble1 = bubble2;
	}
	result[0] = 0;
	return result;
}

double* TCross_Parallel_v1::matvec(double* &x)
{
//	printf( "matvec is working\n");
//	printf("sizes %d x %d\n", this->rows_number, this->columns_number);
// allocate memory
	double *buff  = (double*) malloc( (this->get_rank()) * sizeof(double) );
	double *buff1 = (double*) malloc( (this->rows_number) * sizeof(double) );
// compute V*x
	#pragma omp parallel for
	for (int i = (int (0)); i < this->get_rank(); i++)
	{
		buff[i] = 0.0;
		double s = 0.0;
//		#pragma omp parallel for reduction(+:s)
		for (int j = (int (0)); j < this->columns_number; j++)
		{
			s += V[this->columns_number * i + j] * x[j];
		}
		buff[i] = s;
	}
	
// compute U * (V*x)
	
	#pragma omp parallel for
	for (int i = (int (0)); i < this->rows_number; i++)
	{
		buff1[i]=0.0;
		for(int j = (int (0)); j < this->get_rank(); j++)
		{
			buff1[i] += U[this->rows_number * j + i] * buff[j];
		}
	}
	
//free memory
	free(buff);
	buff = NULL;
	return buff1;
}

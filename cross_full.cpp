#include "parallel_cross_omp.h"
#include <iostream>

using namespace std;

TCross_Full_Work_Data::TCross_Full_Work_Data(TMatrix *original_matrix, TMatrix * d)
{
    int m = original_matrix->get_rows_number();
    int n = original_matrix->get_columns_number();
    diff_matrix = (double *) malloc(m * n * sizeof(double));
    #pragma omp parallel for
    for (int j = 0; j < m*n; j++)
    {
        diff_matrix[j] = original_matrix->value(j % m, j / m);
    }
}

TCross_Full_Work_Data::~TCross_Full_Work_Data()
{
    free(diff_matrix);
}

void TCross_Full::Prepare_Data(TCross_Full_Work_Data &work_data, const TCross_Parallel_v1_Parameters & parameters)
{
    norm = 0.0;
    free(U);
    free(V);
    U = NULL;
    V = NULL;
    free(rows_numbers);
    free(columns_numbers);
    rows_numbers = NULL;
    columns_numbers = NULL;
    tolerance = parameters.tolerance;
    work_data.parameters = parameters;
    if (work_data.parameters.max_rank <= ((int) 0))
    {
        work_data.parameters.max_rank = 1;
    }
    if ((work_data.parameters.rank_increase) <= ((int) 0) && (work_data.parameters.memory_strategy))
    {
        work_data.parameters.rank_increase = 1;
    }
    else if ((work_data.parameters.rank_increase) <= 1 && (!work_data.parameters.memory_strategy))
    {
        work_data.parameters.rank_increase = 1 + 1;
    }
    #pragma omp parallel sections
    {
        #pragma omp section
            U = (double *) calloc(work_data.parameters.max_rank * this->get_rows_number(), sizeof(double));
        #pragma omp section
            V = (double *) calloc(work_data.parameters.max_rank * this->get_columns_number(), sizeof(double));
        #pragma omp section
            rows_numbers = (int *) realloc(rows_numbers, work_data.parameters.max_rank*sizeof(int));
        #pragma omp section
            columns_numbers = (int *) realloc(columns_numbers, work_data.parameters.max_rank*sizeof(int));
    }
    this->rank = ((int) 0);
}

TCross_Full::TCross_Full(): TCross_Base<TCross_Full_Work_Data, TCross_Parallel_v1_Parameters>()
{
    U = NULL;
    V = NULL;
    rows_numbers = NULL;
    columns_numbers = NULL;
    this->rank = ((int) 0);
};

TCross_Full::~TCross_Full()
{
    free(U);
    free(V);
    free(rows_numbers);
    free(columns_numbers);
};

double TCross_Full::value(const int &i, const int &j)
{
    return dot_u(this->rank, U + i, this->get_rows_number(), V + j, this->get_columns_number());
}

void TCross_Full::Search_Max_Volume(TCross_Full_Work_Data &work_data)
{
    if (this->rank == min(this->get_rows_number(), this->get_columns_number())) return;
    if (this->rank >= work_data.parameters.max_rank)
    {
        if (work_data.parameters.memory_strategy)
        {
            work_data.parameters.max_rank += work_data.parameters.rank_increase;
        }
        else
        {
            work_data.parameters.max_rank *= work_data.parameters.rank_increase;
        }
        #pragma omp parallel sections
        {
            #pragma omp section
            {
                U = (double *) realloc(U, work_data.parameters.max_rank * this->get_rows_number() * sizeof(double));
                for (int i = rank*this->get_rows_number(); i < work_data.parameters.max_rank * this->get_rows_number(); i++) U[i] = 0.0;
            }
            #pragma omp section
            {
                V = (double *) realloc(V, work_data.parameters.max_rank * this->get_columns_number() * sizeof(double));
                for (int i = rank*this->get_columns_number(); i < work_data.parameters.max_rank * this->get_columns_number(); i++) V[i] = 0.0;
            }
            #pragma omp section
                rows_numbers = (int *) realloc(rows_numbers, work_data.parameters.max_rank*sizeof(int));
            #pragma omp section
                columns_numbers = (int *) realloc(columns_numbers, work_data.parameters.max_rank*sizeof(int));
        }
        #pragma omp barrier	
    }
    work_data.i = iamax(this->get_columns_number()*this->get_rows_number(), work_data.diff_matrix, 1);
    work_data.j = work_data.i / this->get_rows_number();
    work_data.i = work_data.i % this->get_rows_number();
}

bool TCross_Full::Stopping_Criteria(TCross_Full_Work_Data &work_data){
    double err_nrm = nrm2(this->get_columns_number()*this->get_rows_number(), work_data.diff_matrix, 1);
    if (((work_data.parameters.stop_rank > 0) && (this->rank >= work_data.parameters.stop_rank)) ||(sqrt(real(norm))*abs(tolerance) >= err_nrm))
    {
        #pragma omp parallel sections
        {
            #pragma omp section
                U = (double *) realloc(U, this->rank * this->get_rows_number() * sizeof(double));
            #pragma omp section
                V = (double *) realloc(V, this->rank * this->get_columns_number() * sizeof(double));
            #pragma omp section
                rows_numbers = (int *) realloc(rows_numbers ,this->rank*sizeof(int));
            #pragma omp section
                columns_numbers = (int *) realloc(columns_numbers,this->rank*sizeof(int));
        }
        #pragma omp barrier
        return true;
    }
    else
    {
        return false;
    }
}

void TCross_Full::Update_Cross(TCross_Full_Work_Data &work_data)
{
    double column_factor(1.0/sqrt(abs(work_data.diff_matrix[work_data.i + get_rows_number() * work_data.j]))), row_factor = 1.0 / (work_data.diff_matrix[work_data.i + get_rows_number() * work_data.j] * column_factor);
    rows_numbers[this->rank] = work_data.i;
    columns_numbers[this->rank] = work_data.j;
    axpy(this->get_rows_number(), column_factor, work_data.diff_matrix + work_data.j * this->get_rows_number(), 1, U + rank * this->get_rows_number(), 1);
    axpy(this->get_columns_number(), row_factor, work_data.diff_matrix + work_data.i, this->get_rows_number(), V + rank * this->get_columns_number(), 1);
    gemm(CblasColMajor, CblasNoTrans, CblasTrans, this->get_rows_number(), this->get_columns_number(), 1, -1.0, U + rank * this->get_rows_number(), this->get_rows_number(), V + rank * this->get_columns_number(), this->get_columns_number(), 1.0, work_data.diff_matrix, this->get_rows_number());
    double *b = (double *) calloc ((this->rank + 1), sizeof(double)), *x = (double *) calloc ((this->rank + 1), sizeof(double));
    gemv(CblasColMajor, CblasConjTrans, this->get_rows_number(), this->rank + 1, 1.0, U, this->get_rows_number(), U + rank * get_rows_number(), 1, 0.0, b, 1);
    gemv(CblasColMajor, CblasConjTrans, this->get_columns_number(), this->rank + 1, 1.0, V, this->get_columns_number(), V + rank * get_columns_number(), 1, 0.0, x, 1);
    norm += 2.0*real(dot_c(this->rank, b, 1, x, 1)) + b[this->rank]*x[this->rank];
    free(x);
    free(b);
    this->rank++;
}

int TCross_Full::get_row_number(const int & k) const
{
    return rows_numbers[k];
}

int TCross_Full::get_column_number(const int & k) const
{
    return columns_numbers[k];
}

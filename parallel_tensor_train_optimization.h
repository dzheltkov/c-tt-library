#ifndef PARALLEL_TENSOR_TRAIN_OPTIMIZATION_H
#define PARALLEL_TENSOR_TRAIN_OPTIMIZATION_H
#include <mpi.h>
#include "tensor_train.h"
#include "tensor.h"
#include <cstdlib>
#include "parallel_cross_omp.h"
#include <cmath>
#include <algorithm>
#include "blas.h"
#include <gsl/gsl_multimin.h>
#include <iostream>
using namespace std;

double MPIMinimize(gsl_multimin_function &, double *, double *, int, const TTensorTrainParameters &, double *, int = 0, int = 0, MPI_Comm = MPI_COMM_WORLD);

class TMPIMinimizeTensor: public TTensor
{
    private:
        double *left_boundary, *right_boundary;
        gsl_multimin_function minex;
        double minimum, shift;
        double * minimum_coordinates;
        double * omp_minimum, ** omp_minimum_coordinates;
    public:
        ~TMPIMinimizeTensor();
        double operator[](const int *);
        void Local_Minimization();
        double get_minimum();
        const double* get_minimum_coordinates();
        void update_shift()
        {
            for (int i = 0; i < omp_get_max_threads(); i++)
            {
                if (omp_minimum[i] < minimum)
                {
                    minimum = omp_minimum[i];
                    double * tmp = minimum_coordinates;
                    minimum_coordinates = omp_minimum_coordinates[i];
                    omp_minimum_coordinates[i] = tmp;
                }
            }
            for (int i = 0; i < omp_get_max_threads(); i++)
            {
                omp_minimum[i] = minimum;
            }
            shift = minimum;
            cout << "Minimum " << shift << endl;
        }
        TMPIMinimizeTensor(gsl_multimin_function &, double *, double *, int);
        void local_minimization(int, const int *, const int *, int *, int *);
        void local_minimization(const int *, int *);
};
#endif

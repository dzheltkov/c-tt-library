.SUFFIXES: .cpp .o

include Makefile.in
DEPS    =  blas_double.cpp blas_int.cpp \
		   cross.cpp matrix.cpp parallel_cross_omp.cpp cross_full.cpp\
		   tensor.cpp tensor_train.cpp \
		   tensor_train_optimization.cpp tensor_train_optimization_qtt.cpp\
		   maximizing_functional.cpp\
		   mpi_cross_full.cpp\
		   parallel_tensor_train_optimization.cpp parallel_tensor_train_optimization_qtt.cpp


OBJS    = $(DEPS:.cpp=.o)

.cpp.o:
	$(CXX) -c  $(COPT) $< $(LIBS)

lib:    $(OBJS)
	$(AR) rc libtt.a $(OBJS)
	ranlib libtt.a

clean:
	rm -f $(OBJS) libtt.a

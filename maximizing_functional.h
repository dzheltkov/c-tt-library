#ifndef MAXIMIZING_FUNCTIONAL_H
#define MAXIMIZING_FUNCTIONAL_H

class TMaximizingFunctional
{
    private:
    protected:
        double shift;
    public:
        virtual double value(const double *) const = 0;
        virtual double local_optimization(const double *, double *, int) = 0;
        virtual void update_shift(const double &);
        virtual double print_value(const double &) const;
        TMaximizingFunctional();
        ~TMaximizingFunctional();
};
#endif

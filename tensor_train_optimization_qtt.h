#ifndef TENSOR_TRAIN_OPTIMIZATION_QTT_H
#define TENSOR_TRAIN_OPTIMIZATION_QTT_H
#include "tensor_train.h"
#include "tensor.h"
#include <cstdlib>
#include "parallel_cross_omp.h"
#include <cmath>
#include <algorithm>
#include "blas.h"
#include <iostream>
using namespace std;

double MinimizeQTT(int td, double (*f) (const double *), double (*m) (const double *, double *), double *left, double *right, int qtt_modes, const TTensorTrainParameters & parameters, double *minimum_positions, int &local_minimums_number, double * &local_minimums, int band_width = 2, int seed = 0);

class TMinimizeTensorQTT: public TTensor
{
    private:
        double *left_boundary, *right_boundary;
        double (*function) (const double *x);
        double (*point_optimization) (const double * point, double * optimized_point);
        double minimum, shift;
        double * minimum_coordinates;
        double * local_minimums;
        int local_minimums_number;
        double * omp_minimum, ** omp_minimum_coordinates;
        int *stop_modes;
        int true_dimensionality;
        int * true_modes_sizes;
    public:
        ~TMinimizeTensorQTT();
        double operator[](const int *);
        double get_minimum();
        const double* get_minimum_coordinates();
        void update_minimum()
        {
            for (int i = 0; i < omp_get_max_threads(); i++)
            {
                if (omp_minimum[i] < minimum)
                {
                    minimum = omp_minimum[i];
                    double * tmp = minimum_coordinates;
                    minimum_coordinates = omp_minimum_coordinates[i];
                    omp_minimum_coordinates[i] = tmp;
                }
            }
            for (int i = 0; i < omp_get_max_threads(); i++)
            {
                omp_minimum[i] = minimum;
            }
            cout << "Minimum " << minimum << endl << "Positions:" << endl;
            for (int i = 0; i < true_dimensionality; i++)
            {
                cout << minimum_coordinates[i] << " ";
            }
            cout << endl << endl;
        }
        void update_shift()
        {
            update_minimum();
            shift = minimum;
        }
        TMinimizeTensorQTT(int td, double (*f) (const double *), double (*m) (const double *, double *), double *left, double *right, int m_s);
        void local_minimization(const int *, int *);
        double * get_local_minimums() {return local_minimums;}
        int get_local_minimums_number() {return local_minimums_number;}
        int get_true_dimensionality() { return true_dimensionality;}
};
#endif

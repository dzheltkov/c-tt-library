#ifndef PARALLEL_TENSOR_TRAIN_OPTIMIZATION_QTT_H
#define PARALLEL_TENSOR_TRAIN_OPTIMIZATION_QTT_H
#include <mpi.h>
#include "tensor_train.h"
#include "tensor.h"
#include <cstdlib>
#include "mpi_cross_full.h"
#include "maximizing_functional.h"
#include <cmath>
#include <algorithm>
#include "blas.h"
#include <iostream>
using namespace std;

double MPIMaximizeQTT(int td, TMaximizingFunctional *, double *left, double *right, int qtt_modes, const TTensorTrainParameters & parameters, double *maximum_positions, int &local_maximums_number, double * &local_maximums, int band_width = 2, int seed = 0, MPI_Comm comm = MPI_COMM_WORLD);

class TMPIMaximizeTensorQTT: public TTensor
{
    private:
        TMaximizingFunctional *functional;
        double *left_boundary, *right_boundary;
        double maximum;
        double * maximum_coordinates;
        double * omp_maximum, ** omp_maximum_coordinates;
        double * local_maximums;
        int local_maximums_number;
        int *stop_modes;
        int true_dimensionality;
        int * true_modes_sizes;
        MPI_Comm comm;
        int comm_size, comm_rank;
    public:
        ~TMPIMaximizeTensorQTT();
        double operator[](const int *);
        double get_maximum();
        double get_omp_maximum(int i) const
        {
            return omp_maximum[i];
        }
        const double* get_maximum_coordinates();
        void update_maximum()
        {
            for (int i = 0; i < omp_get_max_threads(); i++)
            {
                if (omp_maximum[i] > maximum)
                {
                    maximum = omp_maximum[i];
                    double * tmp = maximum_coordinates;
                    maximum_coordinates = omp_maximum_coordinates[i];
                    omp_maximum_coordinates[i] = tmp;
                }
            }
            double maximums[comm_size];
            MPI_Allgather(&maximum, 1, MPI_DOUBLE, maximums, 1, MPI_DOUBLE, comm);
            int k = 0;
            for (int i = 1; i < comm_size; i++)
            {
                if (maximums[i] > maximums[k])
                {
                    k = i;
                }
            }
            maximum = maximums[k];
            MPI_Bcast(maximum_coordinates, true_dimensionality, MPI_DOUBLE, k, comm);
            for (int i = 0; i < omp_get_max_threads(); i++)
            {
                omp_maximum[i] = maximum;
            }
            if (comm_rank == 0)
            {
                cout << "Value " << functional->print_value(maximum) << endl << "Positions:" << endl;
                for (int i = 0; i < true_dimensionality; i++)
                {
                    cout << maximum_coordinates[i] << " ";
                }
                cout << endl << endl;
            }
            MPI_Barrier(comm);
        }
        void update_shift()
        {
            update_maximum();
            functional->update_shift(functional->print_value(maximum));
            maximum = functional->value(maximum_coordinates);
            for (int i = 0; i < omp_get_max_threads(); i++)
            {
                omp_maximum[i] = maximum;
            }
        }
        TMPIMaximizeTensorQTT(int td, TMaximizingFunctional *, double *left, double *right, int m_s, MPI_Comm c);
        void local_maximization(const int *, int *, int = 500);
        double * get_local_maximums() {return local_maximums;}
        int get_local_maximums_number() {return local_maximums_number;}
        int get_true_dimensionality() { return true_dimensionality;}
};
#endif

#include "blas.h"

void orthofactors(const int & m, const int &n, complex<double> * &a, complex<double> * &U, complex<double> * &VT)
{
    double *sigma, *superb;
    if (m <= n)
    {
        U = (complex<double> *) malloc(m * m * sizeof(complex<double>));
        VT = (complex<double> *) malloc(m * n * sizeof(complex<double>));
        sigma = (double *) malloc(m * sizeof(double));
        superb = (double *) malloc((m - 1) * sizeof(double));
        gesvd(LAPACK_COL_MAJOR, 'S', 'S', m, n, a, m, sigma, U, m, VT, m, superb);
        for (int i = 0; i < m; i++)
        {
            scal(m, sigma[i], U + i * m, 1);
        }
    }
    else
    {
        U = (complex<double> *) malloc(m * n * sizeof(complex<double>));
        VT = (complex<double> *) malloc(n * n * sizeof(complex<double>));
        sigma = (double *) malloc(n * sizeof(double));
        superb = (double *) malloc((n - 1) * sizeof(double));
        gesvd(LAPACK_COL_MAJOR, 'S', 'S', m, n, a, m, sigma, U, m, VT, m, superb);
        for (int i = 0; i < n; i++)
        {
            scal(n, sigma[i], VT + i, n);
        }
    }
    free(sigma);
    free(superb);
    return;
}


void reducedorthofactors(const double &tol, const int & m, const int &n, complex<double> * &a, complex<double> * &U, complex<double> * &VT, int &newrank)
{
    double *sigma, *superb;
    if (m <= n)
    {
        U = (complex<double> *) malloc(m * m * sizeof(complex<double>));
        VT = (complex<double> *) malloc(m * n * sizeof(complex<double>));
        sigma = (double *) malloc(m * sizeof(double));
        superb = (double *) malloc((m - 1) * sizeof(double));
        gesvd(LAPACK_COL_MAJOR, 'S', 'S', m, n, a, m, sigma, U, m, VT, m, superb);
        double s = 0;
        #pragma omp parallel for reduction (+:s)
        for (int i = 0; i < min(m,n); i++)
        {
            s += sigma[i]*sigma[i];
        }
        #pragma omp barrier
        double r = 0;
        newrank = 0;
        for (int i = m; i > 0; i--)
        {
            r += sigma[i-1]*sigma[i-1];
            if (s*tol*tol <= r)
            {
                newrank = i;
                break;
            }
        }
        for (int i = 0; i < newrank; i++)
        {
            scal(m, sigma[i], U + i * m, 1);
        }
        U = (complex<double> *) realloc(U, m * newrank * sizeof(complex<double>));
        complex<double> * tmp = (complex<double> *) malloc(newrank * n * sizeof(complex<double>));
        for (int i = 0; i < newrank; i++)
        {
            copy(n, VT + i, m, tmp + i, newrank);
        }
        free(VT);
        VT = tmp;
    }
    else
    {
        U = (complex<double> *) malloc(m * n * sizeof(complex<double>));
        VT = (complex<double> *) malloc(n * n * sizeof(complex<double>));
        sigma = (double *) malloc(n * sizeof(double));
        superb = (double *) malloc((n - 1) * sizeof(double));
        gesvd(LAPACK_COL_MAJOR, 'S', 'S', m, n, a, m, sigma, U, m, VT, m, superb);
        double s = 0;
        #pragma omp parallel for reduction (+:s)
        for (int i = 0; i < min(m,n); i++)
        {
            s += sigma[i]*sigma[i];
        }
        #pragma omp barrier
        double r = 0;
        newrank = 0;
        for (int i = n; i > 0; i--)
        {
            r += sigma[i-1]*sigma[i-1];
            if (s*tol*tol <= r)
            {
                newrank = i;
                break;
            }
        }
        complex<double> *tmp = (complex<double> *) malloc(newrank * n * sizeof(complex<double>));
        for (int i = 0; i < newrank; i++)
        {
            scal(n, sigma[i], VT + i, n);
            copy(n, VT + i, n, tmp + i, newrank);
        }
        free(VT);
        VT = tmp;
        U = (complex<double> *) realloc(U, m * newrank * sizeof(complex<double>));
    }
    free(sigma);
    free(superb);
    return;
}


int gesvd(int matrix_order, char jobu, char jobvt, const int m, const int n, complex<double>* a, const int lda, double* s, complex<double>* u, const int ldu, complex<double>* vt, const int ldvt, double* superb)
{
    return LAPACKE_zgesvd(matrix_order, jobu, jobvt, m, n, (lapack_complex_double *) a, lda, s, (lapack_complex_double *) u, ldu, (lapack_complex_double *) vt, ldvt, superb);
}

void copy(const int n, const complex<double> * x, const int incx, complex<double> *y, const int incy)
{
    cblas_zcopy(n, x, incx, y, incy);
    return;
}

void scal(const int & n, const complex<double> & factor, complex<double> * x, const int & incx){
   cblas_zscal(n, &factor, x, incx);
   return;
}

void gemv(const CBLAS_ORDER order, const CBLAS_TRANSPOSE TransA, const int M, const int N, const complex<double> alpha, const complex<double> *A, const int lda, const complex<double> *X, const int incX, const complex<double> beta, complex<double> *Y, const int incY)
{
    cblas_zgemv(order, TransA, M, N, &alpha, A, lda, X, incX, &beta, Y, incY);
    return;
}

void gemm(const CBLAS_ORDER Order, const CBLAS_TRANSPOSE TransA, const CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const complex<double> alpha, const complex<double> *A, const int lda, const complex<double> *B, const int ldb, const complex<double> beta, complex<double> *C, const int ldc)
{
    cblas_zgemm(Order, TransA, TransB, M, N, K, &alpha, A, lda, B, ldb, &beta, C, ldc);
    return;
}

complex<double> dot_c(const int & n, const complex<double> * x, const int & incx, const complex<double> * y, const int & incy){
   complex<double> r;
   cblas_zdotc_sub(n, x, incx, y, incy, &r);
   return r;
}

complex<double> dot_u(const int & n, const complex<double> * x, const int & incx, const complex<double> * y, const int & incy){
   complex<double> r;
   cblas_zdotu_sub(n, x, incx, y, incy, &r);
   return r;
}

using namespace std;
complex<double> * pseudoinverse(double tolerance, int m, int n, complex<double> * A)
{
    if (min(m,n) == 0) return NULL;
    complex<double> * inv_A = (complex<double> *) malloc(n*m*sizeof(complex<double>));
    complex<double> * U = (complex<double> *) malloc(m*min(m, n)*sizeof(complex<double>));
    complex<double> * VT = (complex<double> *) malloc(min(m, n)*n*sizeof(complex<double>));
    double * sigma = (double *) malloc(min(m, n)*sizeof(double));
    double *superb = (double *) malloc((min(m,n)-1)*sizeof(double));
    LAPACKE_zgesvd(LAPACK_COL_MAJOR, 'S', 'S', m, n, (lapack_complex_double *) A, m, sigma, (lapack_complex_double *) U, m, (lapack_complex_double *) VT, min(m, n), superb);
    free(superb);
    double s = 0;
    #pragma omp parallel for reduction (+:s)
    for (int i = 0; i < min(m,n); i++)
    {
        s += sigma[i]*sigma[i];
    }
    #pragma omp barrier
    double r = 0;
    int k = 0;
    for (int i = min(m, n); i > 0; i--)
    {
        r += sigma[i-1]*sigma[i-1];
        if (s*tolerance*tolerance <= r * (1e0l + tolerance*tolerance))
        {
            k = i;
            break;
        }
    }
    #pragma omp parallel for
    for (int i = 0; i < k; i++)
    {
        complex<double> t(1.0/sigma[i]);
        cblas_zscal(n, &t, VT + i, min(m, n));
    }
    #pragma omp barrier
    double d_one = 1.0, d_zero = 0.0;
    cblas_zgemm(CblasColMajor, CblasConjTrans, CblasConjTrans, n, m, k, &d_one, VT, min(m, n), U, m, &d_zero, inv_A, n);
    free(U);
    free(VT);
    free(sigma);
    return inv_A;
}


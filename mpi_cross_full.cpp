#include "mpi_cross_full.h"
#include <iostream>

using namespace std;

TMPICross_Full_Parameters::TMPICross_Full_Parameters():tolerance(0.0), max_rank(1), rank_increase(1), stop_rank(0), memory_strategy(true)
{
    comm = MPI_COMM_WORLD;
}

TMPICross_Full_Work_Data::TMPICross_Full_Work_Data(TMatrix *original_matrix, TMatrix * d)
{
    int m = original_matrix->get_rows_number();
    int n = original_matrix->get_columns_number();
    diff_matrix = (double *) malloc(m * n * sizeof(double));
    matrix = original_matrix;
}

TMPICross_Full_Work_Data::~TMPICross_Full_Work_Data()
{
    free(diff_matrix);
}

void TMPICross_Full::Prepare_Data(TMPICross_Full_Work_Data &work_data, const TMPICross_Full_Parameters & parameters)
{
    free(U);
    free(V);
    U = NULL;
    V = NULL;
    free(rows_numbers);
    free(columns_numbers);
    rows_numbers = NULL;
    columns_numbers = NULL;
    tolerance = parameters.tolerance;
    work_data.parameters = parameters;
    if (work_data.parameters.max_rank <= ((int) 0))
    {
        work_data.parameters.max_rank = 1;
    }
    if ((work_data.parameters.rank_increase) <= ((int) 0) && (work_data.parameters.memory_strategy))
    {
        work_data.parameters.rank_increase = 1;
    }
    else if ((work_data.parameters.rank_increase) <= 1 && (!work_data.parameters.memory_strategy))
    {
        work_data.parameters.rank_increase = 1 + 1;
    }
    #pragma omp parallel sections
    {
        #pragma omp section
            U = (double *) calloc(work_data.parameters.max_rank * this->get_rows_number(), sizeof(double));
        #pragma omp section
            V = (double *) calloc(work_data.parameters.max_rank * this->get_columns_number(), sizeof(double));
        #pragma omp section
            rows_numbers = (int *) realloc(rows_numbers, work_data.parameters.max_rank*sizeof(int));
        #pragma omp section
            columns_numbers = (int *) realloc(columns_numbers, work_data.parameters.max_rank*sizeof(int));
    }
    this->rank = ((int) 0);
    int comm_size, comm_rank;
    MPI_Comm_size(work_data.parameters.comm, &comm_size);
    MPI_Comm_rank(work_data.parameters.comm, &comm_rank);
    int m = work_data.matrix->get_rows_number();
    int n = work_data.matrix->get_columns_number();
    double tmp[m * n];
    for (int j = 0; j < m*n; j++)
    {
        tmp[j] = 0;
    }
    int avg_group_size, group_size, group_num, group_start;
    avg_group_size = (m*n - 1) / comm_size + 1;
    group_num = comm_rank % (m*n);
    if ((avg_group_size == 1) || (group_num < (m*n) % (avg_group_size - 1)))
    {
        group_start = avg_group_size * group_num;
        group_size = avg_group_size;
    }
    else
    {
        group_start = (avg_group_size - 1) * group_num + (m*n) % (avg_group_size - 1);
        group_size = avg_group_size - 1;
    }
    #pragma omp parallel for
    for (int j = group_start; j < group_start + group_size; j++)
    {
        tmp[j] = work_data.matrix->value(j % m, j / m);
    }
    MPI_Allreduce(tmp, work_data.diff_matrix, m*n, MPI_DOUBLE, MPI_SUM, work_data.parameters.comm);
    norm = nrm2(this->get_columns_number()*this->get_rows_number(), work_data.diff_matrix, 1);
}

TMPICross_Full::TMPICross_Full(): TCross_Base<TMPICross_Full_Work_Data, TMPICross_Full_Parameters>()
{
    U = NULL;
    V = NULL;
    rows_numbers = NULL;
    columns_numbers = NULL;
    this->rank = ((int) 0);
};

TMPICross_Full::~TMPICross_Full()
{
    free(U);
    free(V);
    free(rows_numbers);
    free(columns_numbers);
};

double TMPICross_Full::value(const int &i, const int &j)
{
    return dot_u(this->rank, U + i, this->get_rows_number(), V + j, this->get_columns_number());
}

void TMPICross_Full::Search_Max_Volume(TMPICross_Full_Work_Data &work_data)
{
    if (this->rank == min(this->get_rows_number(), this->get_columns_number())) return;
    if (this->rank >= work_data.parameters.max_rank)
    {
        if (work_data.parameters.memory_strategy)
        {
            work_data.parameters.max_rank += work_data.parameters.rank_increase;
        }
        else
        {
            work_data.parameters.max_rank *= work_data.parameters.rank_increase;
        }
        #pragma omp parallel sections
        {
            #pragma omp section
            {
                U = (double *) realloc(U, work_data.parameters.max_rank * this->get_rows_number() * sizeof(double));
                for (int i = rank*this->get_rows_number(); i < work_data.parameters.max_rank * this->get_rows_number(); i++) U[i] = 0.0;
            }
            #pragma omp section
            {
                V = (double *) realloc(V, work_data.parameters.max_rank * this->get_columns_number() * sizeof(double));
                for (int i = rank*this->get_columns_number(); i < work_data.parameters.max_rank * this->get_columns_number(); i++) V[i] = 0.0;
            }
            #pragma omp section
                rows_numbers = (int *) realloc(rows_numbers, work_data.parameters.max_rank*sizeof(int));
            #pragma omp section
                columns_numbers = (int *) realloc(columns_numbers, work_data.parameters.max_rank*sizeof(int));
        }
        #pragma omp barrier	
    }
    work_data.i = iamax(this->get_columns_number()*this->get_rows_number(), work_data.diff_matrix, 1);
    work_data.j = work_data.i / this->get_rows_number();
    work_data.i = work_data.i % this->get_rows_number();
}

bool TMPICross_Full::Stopping_Criteria(TMPICross_Full_Work_Data &work_data){
    double err_nrm = nrm2(this->get_columns_number()*this->get_rows_number(), work_data.diff_matrix, 1);
    if (((work_data.parameters.stop_rank > 0) && (this->rank >= work_data.parameters.stop_rank)) ||(norm*abs(tolerance) >= err_nrm))
    {
        #pragma omp parallel sections
        {
            #pragma omp section
                U = (double *) realloc(U, this->rank * this->get_rows_number() * sizeof(double));
            #pragma omp section
                V = (double *) realloc(V, this->rank * this->get_columns_number() * sizeof(double));
            #pragma omp section
                rows_numbers = (int *) realloc(rows_numbers ,this->rank*sizeof(int));
            #pragma omp section
                columns_numbers = (int *) realloc(columns_numbers,this->rank*sizeof(int));
        }
        #pragma omp barrier
        return true;
    }
    else
    {
        return false;
    }
}

void TMPICross_Full::Update_Cross(TMPICross_Full_Work_Data &work_data)
{
    double column_factor(1.0/sqrt(abs(work_data.diff_matrix[work_data.i + get_rows_number() * work_data.j]))), row_factor = 1.0 / (work_data.diff_matrix[work_data.i + get_rows_number() * work_data.j] * column_factor);
    rows_numbers[this->rank] = work_data.i;
    columns_numbers[this->rank] = work_data.j;
    axpy(this->get_rows_number(), column_factor, work_data.diff_matrix + work_data.j * this->get_rows_number(), 1, U + rank * this->get_rows_number(), 1);
    axpy(this->get_columns_number(), row_factor, work_data.diff_matrix + work_data.i, this->get_rows_number(), V + rank * this->get_columns_number(), 1);
    gemm(CblasColMajor, CblasNoTrans, CblasTrans, this->get_rows_number(), this->get_columns_number(), 1, -1.0, U + rank * this->get_rows_number(), this->get_rows_number(), V + rank * this->get_columns_number(), this->get_columns_number(), 1.0, work_data.diff_matrix, this->get_rows_number());
    this->rank++;
}

int TMPICross_Full::get_row_number(const int & k) const
{
    return rows_numbers[k];
}

int TMPICross_Full::get_column_number(const int & k) const
{
    return columns_numbers[k];
}

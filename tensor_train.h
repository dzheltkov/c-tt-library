#include "tensor.h"
#include <cstdlib>
#include <cstdio>
#include "parallel_cross_omp.h"
#include <cmath>
#include <algorithm>
#include "blas.h"

using namespace std;

struct TTensorTrainParameters
{
    public:
        TTensorTrainParameters();
        double tolerance;
        int maximal_iterations_number, stop_rank, cross_max_iterations;
};

class TTensorTrain : public TTensor
{
    private:
        int *ranks;
        double **carriages;
    public:
        double operator[](const int *);
        void Approximate(TTensor *tensor, const TTensorTrainParameters &);
        int get_rank(int &);
        void save_to_file(char *);
        void load_from_file(char *);
        //void Orthogonalize();
        //void Compress(const double &);
        double nrm2();
        double dot(const TTensorTrain &);
        TTensorTrain();
        TTensorTrain(const TTensorTrain &, const  double &);
        TTensorTrain(const TTensorTrain &tens, const  double *c);
        ~TTensorTrain();
        void SVDCompress (const double = 0.0, const int = 0);
};


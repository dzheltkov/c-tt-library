#include "maximizing_functional.h"

TMaximizingFunctional::TMaximizingFunctional():shift(0)
{
}


TMaximizingFunctional::~TMaximizingFunctional()
{
}


void TMaximizingFunctional::update_shift(const double & s)
{
    shift = s;
}


double TMaximizingFunctional::print_value(const double & v) const
{
    return v;
}

#ifndef MPI_CROSS_FULL_H
#define MPI_CROSS_FULL_H
#include "cross.h"
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include "blas.h"
#include <omp.h>
#include <mpi.h>

struct TMPICross_Full_Parameters
{
    double tolerance;
    int max_rank, rank_increase, stop_rank;
    bool memory_strategy;
    MPI_Comm comm;
    TMPICross_Full_Parameters();
};

struct TMPICross_Full_Work_Data{
    double *diff_matrix;
    TMatrix * matrix;
    int i, j;
    TMPICross_Full_Parameters parameters;
    TMPICross_Full_Work_Data(TMatrix *, TMatrix *);
    ~TMPICross_Full_Work_Data();
};

class TMPICross_Full: public TCross_Base<TMPICross_Full_Work_Data, TMPICross_Full_Parameters>
{
    private:
        double *U, *V, tolerance, norm;
        int * rows_numbers, * columns_numbers;
        void Prepare_Data(TMPICross_Full_Work_Data &, const TMPICross_Full_Parameters &);
        void Search_Max_Volume(TMPICross_Full_Work_Data &);
        bool Stopping_Criteria(TMPICross_Full_Work_Data &);
        void Update_Cross(TMPICross_Full_Work_Data &);
    public:
        double value(const int &, const int &);
        int get_row_number(const int &) const;
        int get_column_number(const int &) const;
        TMPICross_Full();
        ~TMPICross_Full();
};
#endif

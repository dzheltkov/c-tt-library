#include "parallel_tensor_train_optimization.h"
#include <iostream>
#include <limits>
#include <mkl.h>

bool array_compare(int size, int* a, int *b)
{
   for (int i = 0; i < size; i++)
   {
       if (a[i] != b[i]) return false;
   }
   return true;
}

void exclude_dublicates(int & number, const int & size, const int & start, int * a, const int & step, const int & shift = 0)
{
    if (step < size)
    {
        cout << "Exclude dublicates ERROR: step can't be smaller then size" << endl;
        exit(-1);
    }
    int l = start;
    while (l < number)
    {
        for (int i = 0; i < l; i++)
        {
            if (array_compare(size, a + step*i + shift, a + step*l + shift))
            {
                number--;
                if (l != number) copy(size, a + step*number + shift, 1, a + step*l + shift, 1);
                l--;
                break;
            }
        }
        l++;
    }
    return;
}

void exclude_dublicates_compute_positions(int & number, int * positions, const int & pos_step, const int & size, int * a, const int & step, const int & shift = 0)
{
    if (step < size)
    {
        cout << "Exclude dublicates ERROR: step can't be smaller then size" << endl;
        exit(-1);
    }
    int prev_positions[number];
    for (int i = 0; i < number; i++)
    {
        positions[i * pos_step] = i;
        prev_positions[i] = i;
    }
    int l = 1;
    while (l < number)
    {
        for (int i = 0; i < l; i++)
        {
            if (array_compare(size, a + step*i + shift, a + step*l + shift))
            {
                number--;
                if (l != number) copy(size, a + step*number + shift, 1, a + step*l + shift, 1);
                positions[prev_positions[l] * pos_step] = i;
                positions[prev_positions[number] * pos_step] = l;
                prev_positions[l] = prev_positions[number];
                l--;
                break;
            }
        }
        l++;
    }
    return;
}

void TMPIMinimizeTensor::local_minimization(int k, const int * left, const int * right, int * new_left, int * new_right)
{
    const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2;
    gsl_multimin_fminimizer *s = NULL;
    gsl_vector *ss, *x;
    size_t iter = 0;
    int status;
    double size;
    x = gsl_vector_alloc (dimensionality);
    for (int i = 0; i < k; i++)
    {
        gsl_vector_set (x, i, left_boundary[i] + (right_boundary[i] - left_boundary[i]) * ((double) left[i]) / (this->get_mode_size(i) - 1));
    }
    for (int i = k; i < dimensionality; i++)
    {
        gsl_vector_set (x, i, left_boundary[i] + (right_boundary[i] - left_boundary[i]) * ((double) right[i - k]) / (this->get_mode_size(i) - 1));
    }
    ss = gsl_vector_alloc (dimensionality);
    double min_step =  (right_boundary[0] - left_boundary[0]) / (this->get_mode_size(0) - 1);
    for (int i = 0; i < dimensionality; i++)
    {
        double step =  (right_boundary[i] - left_boundary[i]) / (this->get_mode_size(i) - 1);
        gsl_vector_set (ss, i, step / 2);
        if (step < min_step) min_step = step;
    }
    s = gsl_multimin_fminimizer_alloc (T, dimensionality);
    gsl_multimin_fminimizer_set (s, &minex, x, ss);
    do
    {
        iter++;
        status = gsl_multimin_fminimizer_iterate(s);
        if (status) 
            break;
        size = gsl_multimin_fminimizer_size (s);
        status = gsl_multimin_test_size (size, min_step / 10);
    }
    while ((status == GSL_CONTINUE) && (iter < 1000));
    double r = s->fval;
    if (r < omp_minimum[omp_get_thread_num()])
    {
        omp_minimum[omp_get_thread_num()] = r;
        for (int i = 0; i < dimensionality; i++)
        {
            omp_minimum_coordinates[omp_get_thread_num()][i] = gsl_vector_get(s->x, i);
            while (omp_minimum_coordinates[omp_get_thread_num()][i] > right_boundary[i])  omp_minimum_coordinates[omp_get_thread_num()][i] -= right_boundary[i] - left_boundary[i];
            while (omp_minimum_coordinates[omp_get_thread_num()][i] < left_boundary[i])  omp_minimum_coordinates[omp_get_thread_num()][i] += right_boundary[i] - left_boundary[i];
        }
    }
    for (int i = 0; i < k; i++)
    {
        new_left[i] = (int) ((gsl_vector_get (s->x, i) - left_boundary[i]) / (right_boundary[i] - left_boundary[i]) * (this->get_mode_size(i) - 1) + 0.5) % get_mode_size(i);
    }
    for (int i = k; i < dimensionality; i++)
    {
        new_right[i - k] = (int) ((gsl_vector_get (s->x, i) - left_boundary[i]) / (right_boundary[i] - left_boundary[i]) * (this->get_mode_size(i) - 1) + 0.5) % get_mode_size(i);
    }
    gsl_vector_free(x);
    gsl_vector_free(ss);
    gsl_multimin_fminimizer_free (s);
}

void TMPIMinimizeTensor::local_minimization(const int * point, int * optimized_point)
{
    const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2;
    gsl_multimin_fminimizer *s = NULL;
    gsl_vector *ss, *x;
    size_t iter = 0;
    int status;
    double size;
    x = gsl_vector_alloc (dimensionality);
    for (int i = 0; i < dimensionality; i++)
    {
        gsl_vector_set (x, i, left_boundary[i] + (right_boundary[i] - left_boundary[i]) * ((double) point[i]) / (this->get_mode_size(i) - 1));
    }
    ss = gsl_vector_alloc (dimensionality);
    double min_step =  (right_boundary[0] - left_boundary[0]) / (this->get_mode_size(0) - 1);
    for (int i = 0; i < dimensionality; i++)
    {
        double step =  (right_boundary[i] - left_boundary[i]) / (this->get_mode_size(i) - 1);
        gsl_vector_set (ss, i, step / 2);
        if (step < min_step) min_step = step;
    }
    s = gsl_multimin_fminimizer_alloc (T, dimensionality);
    gsl_multimin_fminimizer_set (s, &minex, x, ss);
    do
    {
        iter++;
        status = gsl_multimin_fminimizer_iterate(s);
        if (status) 
            break;
        size = gsl_multimin_fminimizer_size (s);
        status = gsl_multimin_test_size (size, min_step / 100);
    }
    while ((status == GSL_CONTINUE) && (iter < 1000));
    double r = s->fval;
    if (r < omp_minimum[omp_get_thread_num()])
    {
        omp_minimum[omp_get_thread_num()] = r;
        for (int i = 0; i < dimensionality; i++)
        {
            omp_minimum_coordinates[omp_get_thread_num()][i] = gsl_vector_get(s->x, i);
            while (omp_minimum_coordinates[omp_get_thread_num()][i] > right_boundary[i])  omp_minimum_coordinates[omp_get_thread_num()][i] -= right_boundary[i] - left_boundary[i];
            while (omp_minimum_coordinates[omp_get_thread_num()][i] < left_boundary[i])  omp_minimum_coordinates[omp_get_thread_num()][i] += right_boundary[i] - left_boundary[i];
        }
    }
    for (int i = 0; i < dimensionality; i++)
    {
        while (gsl_vector_get (s->x, i) > right_boundary[i]) gsl_vector_set(s->x, i, gsl_vector_get(s->x, i) - right_boundary[i] + left_boundary[i]);
        while (gsl_vector_get (s->x, i) < left_boundary[i]) gsl_vector_set(s->x, i, gsl_vector_get(s->x, i) + right_boundary[i] - left_boundary[i]);
        optimized_point[i] = (int) ((gsl_vector_get (s->x, i) - left_boundary[i]) / (right_boundary[i] - left_boundary[i]) * (this->get_mode_size(i) - 1) + 0.5) % get_mode_size(i);
    }
    gsl_vector_free(x);
    gsl_vector_free(ss);
    gsl_multimin_fminimizer_free (s);
}

double TMPIMinimizeTensor::operator[](const int *i)
{
    gsl_vector *v;
    v = gsl_vector_alloc (dimensionality);
    for (int j = 0;  j < dimensionality; j++)
    {
        gsl_vector_set (v, j, left_boundary[j] + (right_boundary[j] - left_boundary[j]) * ((double) i[j]) / (this->get_mode_size(j) - 1));
    }
    double r = minex.f(v, minex.params);
    if (r < omp_minimum[omp_get_thread_num()])
    {
        omp_minimum[omp_get_thread_num()] = r;
        for (int k = 0; k < dimensionality; k++)
        {
            omp_minimum_coordinates[omp_get_thread_num()][k] = gsl_vector_get(v, k);
        }
    }
    gsl_vector_free(v);
    r -= shift;
    /*if (r > 0)
    {
        return 1.0 - 2.0 * atan(r) / M_PI;
    }
    else if (r < -100)
    {
        return exp(100);
    }
    else
    {
        return exp(-r);
    }*/
    return M_PI/2 - atan(r);
}

TMPIMinimizeTensor::~TMPIMinimizeTensor()
{
    free(minimum_coordinates);
    free(omp_minimum);
    for (int i = 0; i < omp_get_max_threads(); i++)
    {
        free(omp_minimum_coordinates[i]);
    }
    free(omp_minimum_coordinates);
}

double TMPIMinimizeTensor::get_minimum()
{
    return minimum;
}

const double * TMPIMinimizeTensor::get_minimum_coordinates()
{
    return minimum_coordinates;
}

TMPIMinimizeTensor::TMPIMinimizeTensor(gsl_multimin_function &m, double *left, double *right, int m_s)
{
    dimensionality = m.n;
    minex = m;
    shift = 0;
    modes_sizes = (int *) malloc (dimensionality*sizeof(int));
    for (int i = 0; i < dimensionality; i++)
    {
        modes_sizes[i] = m_s;
    }
    left_boundary = left;
    right_boundary = right;
    minimum = numeric_limits<double>::max();
    minimum_coordinates = (double *) malloc(dimensionality*sizeof(double));
    omp_minimum = (double *) malloc(omp_get_max_threads()*sizeof(double));
    omp_minimum_coordinates = (double **) malloc(omp_get_max_threads()*sizeof(double *));
    for (int i = 0; i < omp_get_max_threads(); i++)
    {
        omp_minimum_coordinates[i] = (double *) malloc(dimensionality*sizeof(double));
        omp_minimum[i] = minimum;
    }
}

double MPIMinimize(gsl_multimin_function &minex, double *left, double *right, int modes_size, const TTensorTrainParameters & parameters, double *minimum_positions, int band_width, int seed, MPI_Comm comm)
{
    //MPI parameters obtaining
    int comm_size, comm_rank;
    MPI_Comm_size(comm, &comm_size);
    MPI_Comm_rank(comm, &comm_rank);

    //Constructing tensor
    TMPIMinimizeTensor tensor(minex, left, right, modes_size);
   

    //Obtaining parameters
    int d = tensor.get_dimensionality();
    int max_rank = parameters.stop_rank;
    if (band_width < 2 || band_width > d  - 1)
    {
        band_width = d - 1;
    }

    //Maximal number of best points - every matrix returns max_rank points, points are obtained from band_width matrices, so it's band_width * max_rank points.
    //Those points are locally optimized, so it another band_width * max_rank points.
    //Also generating band_width*max_rank quasirandom points.
    //In total - 3 * band_width * max_rank points.
    int max_number_of_best_points = (3 * band_width + 1) * max_rank;


    //First indeces and last indeces are being concateneted until corresponding matrix size is not large enough
    int left_border = 0, right_border = d-1;
    int k = modes_size;
    while (k <= 2*max_number_of_best_points*modes_size && left_border < right_border - 1)
    {
       k *= modes_size;
       left_border++;
       right_border--;
    }

    //If number of points is small, just calculate all tensor values
    if (left_border > right_border - 1)    {
        cout << "Number of points is to small" << endl;
        //TO DO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return 0;
    }

    //Separating into working communicators
    int avg_group_size, group_size, group_num, group_start;
    avg_group_size = (right_border - left_border - 1) / (comm_size) + 1;
    group_num = comm_rank % (right_border - left_border);
    MPI_Comm work_comm;
    MPI_Comm_split(comm, group_num, comm_rank, &work_comm);
    if (group_num < (right_border - left_border) % (avg_group_size - 1))
    {
        group_start = left_border + avg_group_size * group_num;
        group_size = avg_group_size;
    }
    else
    {
        group_start = left_border + (avg_group_size - 1) * group_num + (right_border - left_border) % (avg_group_size - 1);
        group_size = avg_group_size - 1;

    }

    MPI_Comm gather_comm;
    MPI_Comm_split(comm, comm_rank / (right_border - left_border), comm_rank, &gather_comm);

    if (band_width > right_border - left_border)
    {
        band_width = right_border - left_border;
    }

    //Allocating memory for best points indeces. Every point is d-dimensional, so in total we nee d * max_number_of_best_points integers to store them.
    int **best_points_indeces =  (int **) malloc((right_border - left_border) * sizeof(int*));
    for (int i = 0; i < right_border - left_border; i++)
    {
        best_points_indeces[i] =  (int *) malloc(d * max_number_of_best_points*sizeof(int));
    }

    //Allocating arrays for matrix indeces
    int **left_indeces = (int **) malloc((right_border - left_border)*sizeof(int *));
    int **right_indeces = (int **) malloc((right_border - left_border)*sizeof(int *));
    int **preserved_points = (int **) malloc((right_border - left_border)*sizeof(int *));
    for (int i = left_border; i < right_border; i++)
    {
        left_indeces[i - left_border] = (int *) malloc((i + 1) * max_number_of_best_points * modes_size * sizeof(int));
        right_indeces[i - left_border] = (int *) malloc((d - i - 1) * max_number_of_best_points * modes_size * sizeof(int));
        preserved_points[i - left_border] = (int *) malloc(2 * max_number_of_best_points * sizeof(int));
    }

    int number_of_best_points[right_border - left_border];

    VSLStreamStatePtr quasi_random_stream[right_border - left_border], random_stream[right_border - left_border];
    for (int i = 0; i < right_border - left_border; i++)
    {
        //Creating random and quasi random streams
        vslNewStream(quasi_random_stream + i, VSL_BRNG_NIEDERR, d);
//        vslNewStream(random_stream + i, VSL_BRNG_NONDETERM, VSL_BRNG_RDRAND);
        vslNewStream(random_stream + i, VSL_BRNG_MT2203 + i, seed);
        //At the start we have no points.
        number_of_best_points[i] = 0;
    }


    for (int iters = 0; iters < parameters.maximal_iterations_number; iters++)
    {
        //If not enough points, adding quasi random ones.
        cout << "Generating points" << endl;
        for (int i = 0; i < right_border - left_border; i++)
        {
            while (number_of_best_points[i] < max_number_of_best_points)
            {
                //Generating quasi random points
                viRngUniform( VSL_RNG_METHOD_UNIFORM_STD, quasi_random_stream[i], d * (max_number_of_best_points - number_of_best_points[i]), best_points_indeces[i] + d * number_of_best_points[i], 0, modes_size);
                
                //Exluding the same points
                int k = number_of_best_points[i];
                if (k == 0) k++;
                number_of_best_points[i] = max_number_of_best_points;
                exclude_dublicates(number_of_best_points[i], d, k, best_points_indeces[i], d);
            }
            /*        cout << "Best points" << endl;
                      for (int j = 0; j < number_of_best_points[i]; j++)
                      {
                      for (int k = 0; k < d; k++)
                      {
                      cout << best_points_indeces[i][d * j + k] << " ";
                      }
                      cout << endl;
                      }
                      cout << endl;*/
        }
        //Now we have 3 * band_width * r different points for each mode. Let's generate matrices.

        //Working with i-th unfolding matrix
        for (int i = left_border; i < right_border; i++)
        {
            cout << i << endl;
            //Copying left indeces from best points
            for (int j = 0; j < number_of_best_points[i - left_border]; j++)
            {
                copy(i, best_points_indeces[i - left_border] + d*j, 1, left_indeces[i - left_border] + (i+1)*j, 1);
            }

            //Exluding the same left indeces
            int number_of_left_points = number_of_best_points[i - left_border];
            exclude_dublicates_compute_positions(number_of_left_points, preserved_points[i - left_border], 2, i, left_indeces[i - left_border], i + 1);
            for (int j = 0; j < number_of_best_points[i - left_border]; j++)
            {
                preserved_points[i - left_border][2*j] += best_points_indeces[i - left_border][d*j + i] * number_of_best_points[i - left_border];
            }
            //If not enough left indeces, adding random ones.
            while (number_of_left_points < number_of_best_points[i - left_border])
            {
                //Generating random indeces
                viRngUniform( VSL_RNG_METHOD_UNIFORM_STD, random_stream[i - left_border], (i+1) * (number_of_best_points[i - left_border] - number_of_left_points), left_indeces[i - left_border] + (i+1) * number_of_left_points, 0, modes_size);
                
                //Exluding the same points
                int k = number_of_left_points;
                number_of_left_points = number_of_best_points[i - left_border];
                exclude_dublicates(number_of_left_points, i, k, left_indeces[i - left_border], i + 1);
            }

            //Copying right indeces from best points
            for (int j = 0; j < number_of_best_points[i - left_border]; j++)
            {
                copy(d - i - 2, best_points_indeces[i - left_border] + d*j + i + 2, 1, right_indeces[i - left_border] + (d - i - 1)*j + 1, 1);
            }
            //Exluding the same right indeces
            int number_of_right_points = number_of_best_points[i - left_border];
            exclude_dublicates_compute_positions(number_of_right_points, preserved_points[i - left_border] + 1, 2, d - i - 2, right_indeces[i - left_border], d - i - 1, 1);
            for (int j = 0; j < number_of_best_points[i - left_border]; j++)
            {
                preserved_points[i - left_border][2*j + 1] += best_points_indeces[i - left_border][d*j + i + 1] * number_of_best_points[i - left_border];
            }
            
            //If not enough right indeces, adding random ones.
            while (number_of_right_points < number_of_best_points[i - left_border])
            {
                //Generating random indeces
                viRngUniform( VSL_RNG_METHOD_UNIFORM_STD, random_stream[i - left_border], (d - i - 1) * (number_of_best_points[i - left_border] - number_of_right_points), right_indeces[i - left_border] + (d - i - 1) * number_of_right_points, 0, modes_size);
                
                //Exluding the same points
                int k = number_of_right_points;
                number_of_right_points = number_of_best_points[i - left_border];
                exclude_dublicates(number_of_right_points, d - i - 2, k, right_indeces[i - left_border], d - i - 1, 1);
            }
            //Now just creating from left indeces the indeces with all possible values of the rightest index
            for (int j = 0; j < modes_size; j++)
            {
                for (int k = 0; k < number_of_best_points[i - left_border]; k++)
                {
                    if (j != 0)
                    {
                        copy(i, left_indeces[i - left_border] + (i+1)*k, 1, left_indeces[i - left_border] + (i+1)*k + (i+1)*number_of_best_points[i - left_border]*j, 1);
                    }
                    left_indeces[i - left_border][(i+1)*k + (i+1)*number_of_best_points[i - left_border]*j + i] = j;
                }
            }
            //Now just creating from right indeces the indeces with all possible values of the leftest index
            for (int j = 0; j < modes_size; j++)
            {
                for (int k = 0; k < number_of_best_points[i - left_border]; k++)
                {
                    right_indeces[i - left_border][(d - i - 1)*k + (d - i - 1)*number_of_best_points[i - left_border]*j] = j;
                    if (j != 0)
                    {
                        copy(d - i - 2, right_indeces[i - left_border] + (d - i - 1)*k + 1, 1, right_indeces[i - left_border] + (d - i - 1)*k + (d - i - 1)*number_of_best_points[i - left_border]*j + 1, 1);
                    }
                }
            }
            /*for (int j = 0; j < number_of_best_points[i - left_border]; j++)
            {
                for (int k = 0; k < i + 1; k++)
                {
                    cout << left_indeces[i - left_border][preserved_points[i - left_border][2 * j] * (i + 1) + k] << " ";
                }
                for (int k = 0; k < d - i - 1; k++)
                {
                    cout << right_indeces[i - left_border][preserved_points[i - left_border][2 * j + 1] * (d - i - 1) + k] << " ";
                }
                cout << endl;
            }
            cout << endl;*/
        }
        //Now we have indeces of unfolding matrix, so we could create submatrices and approximate them
        TCross_Parallel_v1 cross_approx;
        TCross_Parallel_v1_Parameters param;
        param.tolerance = (parameters.tolerance / sqrt((double) tensor.get_dimensionality()));
        param.maximal_iterations_number = parameters.cross_max_iterations;
        param.number_of_checked_elements = 1;
        param.stop_rank = parameters.stop_rank;
        param.max_rank = max(param.stop_rank, 1);
        int new_number_of_best_points[right_border - left_border];
        for (int k = left_border; k < right_border; k++)
        {
            int a = 0;
            copy(max_number_of_best_points * d, &a, 0, best_points_indeces[k - left_border], 1);
            if (k >= group_start && k < group_start + group_size)
            {
                cout << k << ", matrix order = " << number_of_best_points[k - left_border]*modes_size << endl;
                param.number_of_preserved_elements = number_of_best_points[k - left_border];
                param.preserved_elements_indeces = preserved_points[k - left_border];
                param.stream = random_stream[k - left_border];
                //Creating unfolding
                TUnfoldingSubMatrix2 work_matrix((TTensor *) &tensor, k + 1, number_of_best_points[k - left_border]*modes_size, number_of_best_points[k - left_border]*modes_size, left_indeces[k - left_border], right_indeces[k - left_border]);
                //Approximation
                cross_approx.Approximate(&work_matrix, param);
                int rank = cross_approx.get_rank();
                cout << "Rank = " << rank << endl << endl;
                //Writing pivots as new best points
                for (int i = 0; i < rank; i++)
                {
                    copy(k+1, left_indeces[k - left_border] + cross_approx.get_row_number(i) * (k + 1), 1, best_points_indeces[k - left_border] + i * d, 1);
                    copy(d - k - 1, right_indeces[k - left_border] + cross_approx.get_column_number(i) * (d - k - 1), 1, best_points_indeces[k - left_border] + i * d + k + 1, 1);
                }
                new_number_of_best_points[k - left_border] = rank;

                int tmp[new_number_of_best_points[k - left_border] * d];
                copy(new_number_of_best_points[k - left_border] * d, &a, 0, tmp, 1);
                //Optimizing new best points
                
#pragma omp parallel for
                for (int o = 0; o < new_number_of_best_points[k - left_border]; o += group_size)
                {
                    tensor.local_minimization(best_points_indeces[k - left_border] + o*d, tmp + o*d);
                }
                MPI_Allreduce(tmp, best_points_indeces[k - left_border] + new_number_of_best_points[k - left_border] * d, new_number_of_best_points[k - left_border] * d, MPI_INT, MPI_SUM, work_comm);
                new_number_of_best_points[k - left_border] *= 2;
                
                //Exluding the same points
                exclude_dublicates(new_number_of_best_points[k - left_border], d, 1, best_points_indeces[k - left_border], d);
                number_of_best_points[k - left_border] = new_number_of_best_points[k - left_border];
            }
            else
            {
                number_of_best_points[k - left_border] = 0;
            }
        }
        //Points exchange
        if (comm_rank < right_border - left_border)
        {
            int tmp[right_border - left_border];
            copy(right_border - left_border, number_of_best_points, 1, tmp, 1);
            MPI_Allreduce(tmp, number_of_best_points, right_border - left_border, MPI_INT, MPI_SUM, gather_comm);
        }
        MPI_Bcast(number_of_best_points, right_border - left_border, MPI_INT, 0, work_comm);
        for (int i = 0; i < right_border - left_border; i++)
        {
            if (comm_rank < right_border - left_border)
            {
                int tmp[number_of_best_points[i]*d];
                copy(number_of_best_points[i] * d, best_points_indeces[i], 1, tmp, 1);
                MPI_Allreduce(tmp, best_points_indeces[i], number_of_best_points[i] * d, MPI_INT, MPI_SUM, gather_comm);
            }
            MPI_Bcast(best_points_indeces[i], number_of_best_points[i] * d, MPI_INT, 0, work_comm);
        }
        for (int k = 0; k < right_border - left_border; k++)
        {
            //Points from left matrices
            for (int i = max(0, k - (band_width - 1) / 2); i < k; i++)
            {
                copy(d * new_number_of_best_points[i], best_points_indeces[i], 1, best_points_indeces[k] + d * number_of_best_points[k], 1);
                number_of_best_points[k] += new_number_of_best_points[i];
            }
            //Points from circlically left matrices
            for (int i = right_border - left_border + k - (band_width - 1) / 2; i < right_border - left_border; i++)
            {
                copy(d * new_number_of_best_points[i], best_points_indeces[i], 1, best_points_indeces[k] + d * number_of_best_points[k], 1);
                number_of_best_points[k] += new_number_of_best_points[i];
            }
            //Points from right matrices
            for (int i = k + 1; i < min(right_border - left_border, k - (band_width - 1) / 2 + band_width); i++)
            {
                copy(d * new_number_of_best_points[i], best_points_indeces[i], 1, best_points_indeces[k] + d * number_of_best_points[k], 1);
                number_of_best_points[k] += new_number_of_best_points[i];
            }
            //Points from circlically right matrices
            for (int i = 0; i < k - (band_width - 1) / 2 + band_width - (right_border - left_border); i++)
            {
                copy(d * new_number_of_best_points[i], best_points_indeces[i], 1, best_points_indeces[k] + d * number_of_best_points[k], 1);
                number_of_best_points[k] += new_number_of_best_points[i];
            }
        }
        int num = 0;
        for (int k = 0; k < right_border - left_border; k++)
        {
            //Exluding the same points
            exclude_dublicates(number_of_best_points[k], d, 1, best_points_indeces[k], d);
            num += number_of_best_points[k];
        }
        int bp[num*d];
        int i = 0;
        for (int k = 0; k < right_border - left_border; k++)
        {
            copy(number_of_best_points[k]*d, best_points_indeces[k], 1, bp + i*d, 1);
            i += number_of_best_points[k];
        }
        exclude_dublicates(num, d, 1, bp, d);
        double v[num];
#pragma omp parallel for
        for (int k = 0; k < num; k++)
        {
            v[k] = tensor[bp + k*d];
        }
        for (int i = 1; i < num; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (v[i] > v[j])
                {
                    double a = v[j];
                    v[j] = v[i];
                    v[i] = a;
                    int tmp[d];
                    copy(d, bp + j*d, 1, tmp, 1);
                    copy(d, bp + i*d, 1, bp + j*d, 1);
                    copy(d, tmp, 1, bp + i*d, 1);
                }
            }
        }
        for (int k = 0; k < right_border - left_border; k++)
        {
            copy(min(max_rank, num), bp, 1, best_points_indeces[k] + d * number_of_best_points[k], 1);
            number_of_best_points[k] += min(max_rank, num);
            //Exluding the same points
            exclude_dublicates(number_of_best_points[k], d, 1, best_points_indeces[k], d);
        }
        tensor.update_shift();
    }


    for (int i = 0; i < tensor.get_dimensionality(); i++)
    {
        minimum_positions[i] = tensor.get_minimum_coordinates()[i];
    }
    for (int i = 0; i < right_border - left_border; i++)
    {   
        vslDeleteStream(quasi_random_stream + i);
        vslDeleteStream(random_stream + i);
    }
    for (int i = left_border; i < right_border; i++)
    {
        free(left_indeces[i - left_border]);
        free(right_indeces[i - left_border]);
        free(preserved_points[i - left_border]);
    }
    free(left_indeces);
    free(right_indeces);
    free(preserved_points);
    free(best_points_indeces);
    return tensor.get_minimum();
}
